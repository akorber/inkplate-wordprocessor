# inkplate-wordprocessor

Use Inkplate6 epaper display esp32 driven to make a wordprocessor with a keyboard, a battery, write text and save it in onboard SD card.

## Inspirations : Mechanical typewriters, Thermal typewriters, Alphasmart, Wordgrinder, Emacs, Vim, Nano...


More in the wiki : https://framagit.org/akorber/inkplate-wordprocessor/-/wikis/home