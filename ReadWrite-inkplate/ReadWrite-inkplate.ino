/*
  SD card read/write
 This example code is in the public domain.

 */

#include "Inkplate.h"            //Include Inkplate library to the sketch
#include "freesans12.h"       //Include custom font created with fonctconvert to encode accents in french
#include "SdFat.h"               //Include library for SD card
Inkplate display(INKPLATE_1BIT); // Create an object on Inkplate library and also set library into 1 Bit mode (BW)
SdFile file;                     // Create SdFile object used for accessing files on SD card

void setup() {
  display.begin();        // Init Inkplate library (you should call this function ONLY ONCE)
  display.clearDisplay(); // Clear frame buffer of display
  display.display();      // Put clear image on display
  display.setFont(&FreeSans12pt8b); // Select new font
  
  display.setCursor(0, 15);   // Set print position at the begining of the screen plus the height of the font
  display.println("Initializing SD card...");
  display.partialUpdate();

  if (!display.sdCardInit()) {
    display.println("Initializing SD card failed !...");
    display.display();
    return;
  }
  display.println("Initializing SD card done");
  display.partialUpdate();
  // open the file. note that only one file can be open at a time,
  // so you have to close this one before opening another.
  
  //myFile = SD.open("test.txt", FILE_WRITE);

  // if the file opened okay, write to it:
  if (file.open("/text3.txt", FILE_WRITE)) {

    //Serial.print("Writing to test.txt...");
    display.println("Writing to text3.txt...");
    display.partialUpdate();
    file.println("C'est officiel ! le d�me g�od�sique du collectif Usinette est couvert !");
    //myFile.println("testing 1, 2, 3.");
    // close the file:
    file.close();
    display.println("Done");
    display.partialUpdate();
    
  } else {
    // if the file didn't open, print an error:
    
    display.println("error opening text3.txt");
    display.display();    
  }

  // re-open the file for reading:
  if (file.open("/text3.txt", O_RDONLY)) {

    // read from the file until there's nothing else in it:
    while (file.available()) {
      //display.clearDisplay();    // Clear everything that is stored in frame buffer of epaper
      int len = file.fileSize(); // Read how big is file that we are opening
      char text[len];            // Array where data from SD card is stored (max 200 chars here)
      file.read(text,len); // Read data from file and save it in text array
      display.print(text);  // Print data/text
      text[len] = 0;        // Put null terminating char at the and of data
      display.display();    // Do a full refresh of display
    }
    // close the file:
    file.close();
  } else {
    // if the file didn't open, print an error:
    
    
    display.println("error opening text3.txt");
    display.display();
  }
}

void loop() {
  // nothing happens after setup
}
